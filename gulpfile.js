var gulp = require('gulp');
var args = require('get-gulp-args')();
var bump = require('gulp-bump');
var git = require('gulp-git');
var fs = require('fs');

var src = ['./package.json']; 
var branch = args.b || 'development';

gulp.task('checkout:branch', function(cb){
  git.checkout(branch, function(err){
    if (err) {throw err;}
    cb();
  });
});

gulp.task('bump', function(cb){
  var params = {};
  if (args.v) {
    params.version = args.v;
  } else if (args.t) {
    params.type = args.t;
  }

  return gulp.src(src)
    .pipe(bump(params))
    .pipe(gulp.dest('./'));
});

gulp.task('commit', ['bump'], function(){
  return gulp.src(src)
    .pipe(git.commit('up version'));
});

gulp.task('push', ['commit'], function(cb){
  git.push('origin', branch, function(err){
    if (err) {throw err;}
    cb();
  });
});

gulp.task('checkout:master', ['push'], function(cb){
  git.checkout('master', function(err){
    if (err) {throw err;}
    cb();
  });
});

gulp.task('merge', ['checkout:master'], function(cb){
  git.merge(branch, function(err){
    if (err) {throw err;}
    cb();
  });
});

gulp.task('push:master', ['merge'], function(cb){
  git.push('origin', 'master', function(err){
    if (err) {throw err;}
    cb();
  });
});

gulp.task('release', ['push:master'], function(cb){
  var version = JSON.parse(fs.readFileSync(src[0])).version;
  console.log('Released version ' + version + '.');
  
  git.checkout(branch, function(err){
    if (err) {throw err;}
    cb();
  });
})