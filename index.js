var contentful = require('contentful');

function getClient(apiKey, spaceId) {
  var client;

  try {
    client = contentful.createClient({
      accessToken: apiKey,
      space: spaceId
    });
  } catch(e) {
    console.error('Could not create Contenful client.', e);
  }

  return client;
}

function getContentTypes(client, filter, order, level, key, locale) {
  return client.getContentTypes().then(function(contentTypes){
    var types = getTypesArray(contentTypes, filter);
    var requests = [];
    for (var i = 0; i < types.length; i++) {
      requests.push(client.getEntries({select: 'fields' ,content_type: types[i], include: level || 1, locale: locale , order: order || 'fields.order'}));
    }

    return Promise.all(requests).then(function(entries){
      return setEntries(entries, types, key);
    }, function(err){
      throw err;
    })
  }, function(err){
    throw err;
  })
}

function getTypesArray(contentTypes, filter) {
  var filteredTypes = [];

  for (var i = 0; i < contentTypes.items.length; i++) {
    var id = contentTypes.items[i].sys.id;
    if (filter.length === 0 || filter.indexOf(id) > -1) {
      filteredTypes.push(id);
    }
  }

  return filteredTypes;
}

function setEntries(entries, types, key) {
  var data = {};

  for (var i = 0; i < entries.length; i++) {
    var collection = entries[i];
    var content = {};

    try {
      for(var j = 0; j < collection.items.length; j++) {
        var id;
        if (key === undefined || key === null) {
          id = collection.items[j].sys.id;
        } else {
          id = collection.items[j].fields[key];
        }
        content[id] = collection.items[j];
      }
    } catch(e) {
      console.log(e);
    }

    data[types[i]] = content;
  }

  return data;
}

module.exports = function(params, cb){
  var client = getClient(params.apiKey, params.spaceId);

  var opts = {
    filter: params.opts.filter || [],
    level: params.opts.level || 1,
    key: params.opts.key || undefined,
    locale: params.locale || 'default',
    order : params.opts.order
  };

  if (cb && cb instanceof Function) {
    getContentTypes(client, opts.filter, opts.order ,opts.level, opts.key, opts.locale).then(function(data){
      cb(undefined, data);
    }, function(err){
      cb(err);
    })
  } else {
    console.error('Expected callback to be a function. Got %d instead.', cb);
  }
}
